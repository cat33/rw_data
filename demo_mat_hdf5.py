def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return f

def method_mat_read(infile):
    """read in any kind of mat file

    :param infile: input file (str)
    :return: mat_file_ref
    """

    import h5py
    from scipy.io import loadmat

    try:
        f = loadmat(infile)
        matlabversion='5'
    except:
        f = h5py.File(infile)
        matlabversion='73'

    if matlabversion=='5':
        variabledictionary=f
        return variabledictionary
    elif matlabversion=='73':
        variabledictionary = {}
        for (k,v) in f.items():
            variabledictionary[k]=f[k]
        return variabledictionary


